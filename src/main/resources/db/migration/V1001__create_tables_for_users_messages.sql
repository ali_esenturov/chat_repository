use `chat`;

CREATE TABLE `users` (
                         `id` INT auto_increment NOT NULL,
                         `name` varchar(20) NOT NULL,
                         `username` varchar(30) NOT NULL,
                         `email` varchar(30) NOT NULL,
                         `password` varchar(60) NOT NULL,
                         `enabled` tinyint NOT NULL,
                         `role` varchar(30) NOT NULL,
                         PRIMARY KEY (`id`)
);

CREATE TABLE `messages` (
                        `id` INT auto_increment NOT NULL,
                        `text` varchar(300) NOT NULL,
                        `user_id` INT NOT NULL,
                        `msg_ldt` DATETIME NOT NULL,
                        PRIMARY KEY (`id`),
                        CONSTRAINT `fk_message_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
);