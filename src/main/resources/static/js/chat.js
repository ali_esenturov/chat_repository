let interval = setInterval("getMessages()", 2000);

async function getMessages(){
    let lastMessage = document.getElementById("messages").lastElementChild;
    let last_id = lastMessage.id;

    let response = await fetch('http://localhost:8081/messages/get/' + last_id)
                    .catch(function (error){
                    });
    if(response.ok){
        let messages = await response.json();
        console.log(messages);
        for(let i = 0; i < messages.length; i++){
            let element = document.createElement('div');
            element.id = messages[i].id;
            element.innerHTML = "<p> <span class='user'>" + messages[i].user + '  </span> <span class="time">' + messages[i].timeOfMessage + ':</span> </p>' +
                '<p>' + messages[i].text + '</p>';
            document.getElementById("messages").appendChild(element);
        }
    }
}

async function addMessage(form){
    let data = new FormData(form);

    await fetch('http://localhost:8081/messages/add',{
        method: 'POST',
        body: data
    });

    document.getElementById("message-from").reset();
}
