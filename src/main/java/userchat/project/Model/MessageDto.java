package userchat.project.Model;


import lombok.Builder;
import lombok.Data;

import java.time.format.DateTimeFormatter;

@Data
@Builder
public class MessageDto {

    private int id;
    private String user;
    private String text;
    private String timeOfMessage;

    public static MessageDto from(Message msg) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy, HH:mm");

        return builder()
                .id(msg.getId())
                .user(msg.getUser().getUsername())
                .timeOfMessage(msg.getTimeOfMessage().format(formatter))
                .text(msg.getText())
                .build();
    }
}
