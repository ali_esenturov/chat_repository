package userchat.project.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "messages")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 300)
    private String text;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @Column(name = "msg_ldt")
    @Builder.Default
    LocalDateTime timeOfMessage = LocalDateTime.now();
}
