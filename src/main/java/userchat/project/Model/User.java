package userchat.project.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "users")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 20)
    private String name;

    @Column(length = 30)
    private String username;

    @Column(length = 30)
    private String email;

    @Column(length = 60)
    private String password;

    @Column
    @Builder.Default
    private boolean enabled = true;

    @Column(length = 30)
    @Builder.Default
    private String role = "USER";
}
