package userchat.project.Repository;

import org.springframework.data.repository.CrudRepository;
import userchat.project.Model.User;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findByUsername(String username);
}
