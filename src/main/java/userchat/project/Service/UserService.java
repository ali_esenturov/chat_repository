package userchat.project.Service;

import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import userchat.project.Model.User;
import userchat.project.Repository.UserRepository;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository ur;
    private final PasswordEncoder encoder;

    public void saveUser(User user){
        user.setPassword(encoder.encode(user.getPassword()));
        ur.save(user);
    }
}
