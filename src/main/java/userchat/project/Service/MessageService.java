package userchat.project.Service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import userchat.project.Model.Message;
import userchat.project.Model.MessageDto;
import userchat.project.Repository.MessageRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MessageService {
    private final MessageRepository mr;

    public void saveMessage(Message msg){
        mr.save(msg);
    }

    public List<MessageDto> getNewMessages(int id){
        return mr.findAllMessagesWithIdMore(id).stream().map(MessageDto::from).collect(Collectors.toList());
    }
}
