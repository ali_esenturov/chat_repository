package userchat.project.Controller;

import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import userchat.project.Model.Message;
import userchat.project.Model.MessageDto;
import userchat.project.Model.User;
import userchat.project.Repository.UserRepository;
import userchat.project.Service.MessageService;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/messages")
public class MessageController {
    private final MessageService mr;
    private final UserRepository userRepository;

    @GetMapping("/get/{last_id}")
    public List<MessageDto> getMessages(@PathVariable("last_id") Integer last_id){
        return mr.getNewMessages(last_id);
    }

    @PostMapping("/add")
    public void addMessage(@RequestParam("text") String text){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User user = userRepository.findByUsername(currentPrincipalName);
        Message msg = new Message();
        msg.setUser(user);
        msg.setText(text);
        mr.saveMessage(msg);
    }
}
