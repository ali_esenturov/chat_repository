package userchat.project.Controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import userchat.project.Model.User;
import userchat.project.Service.UserService;

@Controller
@AllArgsConstructor
public class MainController {
    private final UserService us;

    @GetMapping("/")
    public String getMainPage(Model model){
        return "index";
    }

    @GetMapping("/login")
    public String getLoginPage(Model model){
        return "login";
    }

    @GetMapping("/register")
    public String getRegisterPage(Model model){
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@ModelAttribute("user") User user){
        us.saveUser(user);
        return "redirect:/login";
    }

    @GetMapping("/chat")
    public String getChatPage(Model model){
        return "chat";
    }
}
